import 'dart:async';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

final String _tableName = 'TransactionList';
final String _columnId = '_id';
final String _columnType = 'type';
final String _columnMoney = 'money';
final String _columnAmount = 'amount';
final String _columnDate = 'date';
final String _columnCategory = 'category';
final String _columnNote = 'note';

class TransactionList {
  int id;
  final String type; // deposit, withdraw, buy, sell
  final double money;
  final int amount;
  final String date;
  final String category;
  final String note;

  Map<String, dynamic> toMap() => {
    _columnId: id,
    _columnType: type,
    _columnMoney: money,
    _columnAmount:amount,
    _columnDate: date,
    _columnCategory: category,
    _columnNote: note,
  };

  TransactionList({
    this.id,
    this.type,
    this.money,
    this.amount,
    this.date,
    this.category,
    this.note,
  });

  factory TransactionList.fromMap(Map<String, dynamic> json) => TransactionList(
    id: json[_columnId],
    type: json[_columnType],
    money: json[_columnMoney],
    amount: json[_columnAmount],
    date: json[_columnDate],
    category: json[_columnCategory],
    note: json[_columnNote]
  );
}

class TransactionListProvider {
  TransactionListProvider._();

  static final TransactionListProvider db = TransactionListProvider._();

  Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await initDB();
    return _database;
  }

  initDB() async {
    String path = join(await getDatabasesPath(), "SanqianDB.db");
    return this.open(path);
  }

  // Database db;

  Future open(String path) async {
    return await openDatabase(
      path,
      version: 1,
      onCreate: (Database db, int version) async {
        await db.execute('''
          create table $_tableName (
            $_columnId integer primary key autoincrement,
            $_columnType text not null,
            $_columnMoney real not null,
            $_columnAmount integer default 1 not null,
            $_columnDate text not null,
            $_columnCategory text not null,
            $_columnNote text
          )
        ''');
      }
    );
  }

  Future<TransactionList> create(TransactionList item) async {
    final db = await database;
    item.id = await db.insert(_tableName, item.toMap());
    return item;
  }

  Future<List<TransactionList>> find() async {
    final db = await database;
    var res = await db.query(_tableName);
    List<TransactionList> list =
        res.isNotEmpty ? res.map((c) => TransactionList.fromMap(c)).toList() : [];
    return list;
  }

  Future<TransactionList> findById(int id) async {
    final db = await database;
    var res = await db.query(_tableName, where: "$_columnId = ?", whereArgs: [id]);
    return res.isNotEmpty ? TransactionList.fromMap(res.first) : null;
  }

  Future<List<TransactionList>> findByDate(String date) async {
    print(date);
    final db = await database;
    var res = await db.query(_tableName, where: "$_columnDate like ?", whereArgs: ["%$date%"]);
    List<TransactionList> list =
        res.isNotEmpty ? res.map((c) => TransactionList.fromMap(c)).toList() : [];
    return list;
  }

  Future<int> deleteById(int id) async {
    final db = await database;
    return await db.delete(_tableName, where: '$_columnId = ?', whereArgs: [id]);
  }

  Future<int> update(TransactionList item) async {
    final db = await database;
    return await db.update(_tableName, item.toMap(), where: '$_columnId = ?', whereArgs: [item.id]);
  }
}