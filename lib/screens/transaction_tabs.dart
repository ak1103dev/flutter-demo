import 'package:flutter/material.dart';
import 'package:sanqian/screens/transaction_tabs/income_tab.dart';
import 'package:sanqian/screens/transaction_tabs/asset_tab.dart';
import 'package:sanqian/screens/transaction_tabs/expense_tab.dart';
import 'package:sanqian/database/transaction_list_model.dart';

class TransactionTabs extends StatefulWidget {
  static const String routeName = "/transaction-form";
  final TransactionList item;

  TransactionTabs(this.item);

  @override
  TransactionFormState createState() => new TransactionFormState(item: item);
}

class TransactionFormState extends State<TransactionTabs> with SingleTickerProviderStateMixin {
  TabController controller;

  TransactionFormState({ this.item });
  final item;


  int _getTabIndex() {
    if (item != null) {
      if (item.type == "deposit") {
        return 0;
      } else if (item.type == "withdraw") {
        return 2;
      } else if (item.type == "buy" || item.type == "sell") {
        return 1;
      }
    }
    return 2;
  }

  @override
  void initState() {
    super.initState();

    controller = new TabController(length: 3, vsync: this);
    controller.animateTo(_getTabIndex());
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  TabBar getTabBar() {
    return new TabBar(
      tabs: <Tab>[
        new Tab(
          text: "Income"
        ),
        new Tab(
          text: "Asset"
        ),
        new Tab(
          text: "Expense"
        ),
      ],
      controller: controller,
    );
  }

  TabBarView getTabBarView(var tabs) {
    return new TabBarView(
      children: tabs,
      controller: controller,
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("New"),
        backgroundColor: Colors.amberAccent[400],
        bottom: getTabBar()
      ),
      body: getTabBarView(<Widget>[
        new IncomeTab(item: item),
        new AssetTab(item: item),
        new ExpenseTab(item: item),
      ])
    );
  }
}
