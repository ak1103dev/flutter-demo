import 'package:flutter/material.dart';
import 'package:sanqian/components/transaction_form.dart';

class AssetTab extends StatelessWidget {
  AssetTab({ this.item });
  final item;
  final List _categories = [
    "GSB's Lottery",
    "Stock",
    "Mutual Fund",
    "RMF",
    "LTF",
    "Crypto Currency",
    "Deposit",
    "Device"
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Padding(
          padding: EdgeInsets.all(10.0),
          child: TransactionForm(categories: _categories, tabName: "asset", item: item)
        ),
      ),
    );
  }
}
