import 'package:flutter/material.dart';
import 'package:sanqian/components/transaction_form.dart';

class ExpenseTab extends StatelessWidget {
  ExpenseTab({ this.item });
  final item;
  final List _categories = [
    "Food",
    "Transportation",
    "Internet",
    "Phone",
    "Book",
    "Social Security",
    "Tax",
    "Party",
    "Travel",
    "Card Fee",
    "Home",
    "Thing",
    "Other"
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Padding(
          padding: EdgeInsets.all(10.0),
          child: TransactionForm(categories: _categories, tabName: "expense", item: item)
        ),
      ),
    );
  }
}
