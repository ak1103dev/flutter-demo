import 'package:flutter/material.dart';
import 'package:sanqian/components/transaction_form.dart';

class IncomeTab extends StatelessWidget {
  IncomeTab({ this.item });
  final item;
  final List _categories = [
    "Salary",
    "Wage",
    "Bonus",
    "Interest",
    "Dividend",
    "Borrowing"
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Padding(
          padding: EdgeInsets.all(10.0),
          child: TransactionForm(categories: _categories, tabName: "income", item: item)
        ),
      ),
    );
  }
}
