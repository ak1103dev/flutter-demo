import 'dart:async';

import 'package:flutter/material.dart';
import 'package:sanqian/database/transaction_list_model.dart';
import 'package:sanqian/screens/transaction_tabs.dart';
import 'package:intl/intl.dart';

class DailyScreen extends StatefulWidget {
  _DailyScreenState createState() => _DailyScreenState();
}

class _DailyScreenState extends State<DailyScreen> {
  final Map<String, Color> color = {
    "deposit": Colors.green,
    "withdraw": Colors.red,
    "buy": Colors.red,
    "sell": Colors.green
  };

  DateTime _date = DateTime.now();

  void _nextDay() {
    setState(() {
      _date = _date.add(new Duration(days: 1));
    });
  }

  void _backDay() {
    setState(() {
      _date = _date.subtract(new Duration(days: 1));
    });
  }

  Future _selectDate() async {
    DateTime picked = await showDatePicker(
        context: context,
        initialDate: _date,
        firstDate: new DateTime(2000),
        lastDate: new DateTime(2100),
    );
    if(picked != null) setState(() => _date = picked);
  }

  @override
  Widget build(BuildContext context) {

    return new Scaffold(
      appBar: AppBar(
        title: Row(children: <Widget>[
          IconButton(onPressed: _backDay, icon: Icon(Icons.keyboard_arrow_left)),
          FlatButton(
            onPressed: _selectDate,
            child: Text(DateFormat('dd-MM-yyyy').format(_date), style: TextStyle(color: Colors.white)),
          ),
          IconButton(onPressed: _nextDay, icon: Icon(Icons.keyboard_arrow_right)),
        ]),
        backgroundColor: Colors.amberAccent[400],
      ),
      backgroundColor: Colors.white,
      body: FutureBuilder<List<TransactionList>>(
        future: TransactionListProvider.db.findByDate(DateFormat('yyyy-MM-dd').format(_date)),
        // future: TransactionListProvider.db.find(),
        builder: (BuildContext context, AsyncSnapshot<List<TransactionList>> snapshot) {
          if (snapshot.hasData) {
            return Container(
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 5.0),
                child: ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    TransactionList item = snapshot.data[index];
                    return GestureDetector(
                      onTap: () {
                        // Navigator.of(context).pushNamed('/transaction-form');
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => TransactionTabs(item),
                          )
                        );
                      },
                      child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Row(children: <Widget>[
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: item.note != "" ? <Widget>[
                                Text(item.category, style: TextStyle(fontWeight: FontWeight.bold)),
                                Text(item.note),
                              ] : <Widget>[
                                Text(item.category, style: TextStyle(fontWeight: FontWeight.bold)),
                              ]
                            )
                          ),
                          Expanded(
                            child: Text("\@ ${item.amount.toString()}", textAlign: TextAlign.center),
                          ),
                          Expanded(
                            child: Text(
                              // item.money.toStringAsFixed(2),
                              NumberFormat("###,###,##0.00").format(item.money),
                              textAlign: TextAlign.right,
                              style: TextStyle(color: color[item.type])
                            ),
                          ),
                        ])
                      )
                    );
                  },
                )
              )
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        }
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).pushNamed('/transaction-form');
        },
        tooltip: 'Increment',
        child: Icon(Icons.add),
        backgroundColor: Colors.amberAccent[400],
      ),
    );
  }
}
