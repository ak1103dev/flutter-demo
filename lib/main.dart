import 'package:flutter/material.dart';

import 'package:sanqian/screens/daily_screen.dart';
import 'package:sanqian/screens/more_screen.dart';
import 'package:sanqian/screens/transaction_tabs.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // title: "SANQIAN",
      // theme: ThemeData(
      //   primarySwatch: Colors.amberAccent[400],
      // ),
      home: MyHome(),
      routes: <String, WidgetBuilder>{
        TransactionTabs.routeName: (BuildContext context) => TransactionTabs(null),
      },
    );
  }
}

class MyHome extends StatefulWidget {
  @override
  MyHomeState createState() => new MyHomeState();
}

class MyHomeState extends State<MyHome> with SingleTickerProviderStateMixin {
  TabController controller;

  @override
  void initState() {
    super.initState();

    controller = new TabController(length: 2, vsync: this);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      // appBar: new AppBar(
      //   title: new Text("SANQIAN"),
      //   backgroundColor: Colors.amberAccent[400],
      // ),
      body: new TabBarView(
        children: <Widget>[new DailyScreen(), new MoreSreen()],
        controller: controller,
      ),
      bottomNavigationBar: new Material(
        color: Colors.amberAccent[400],
        child: new TabBar(
          tabs: <Tab>[
            new Tab(
              icon: new Icon(Icons.calendar_today),
              text: 'Daily',
            ),
            new Tab(
              icon: new Icon(Icons.more_horiz),
              text: 'More',
            ),
          ],
          controller: controller,
        ),
      ),
    );
  }
}
