import 'package:flutter/material.dart';
import 'dart:async';
import 'package:intl/intl.dart';
import 'package:sanqian/database/transaction_list_model.dart';

class TransactionForm extends StatefulWidget {
  TransactionForm({ Key key, @required this.categories, @required this.tabName, this.item }): super(key: key);

  final categories;
  final tabName;
  final item;

  @override
  TransactionFormState createState() {
    return TransactionFormState(categories: categories, tabName: tabName, item: item);
  }
}

class TransactionFormState extends State<TransactionForm> {
  TransactionFormState({ this.categories, this.tabName, this.item });

  final item;
  final categories;
  final tabName;

  String _money = "0.00";
  DateTime _date = DateTime.now();
  String _amount = "1";
  String _category;
  String _note = "";
  String _assetType = "buy";

  String _getTransactionType() {
    if (tabName == "income") {
      return "deposit";
    } else if (tabName == "expense") {
      return "withdraw";
    } else {
      return _assetType;
    }
  }

  bool _isMatchType(type) {
    return (tabName == "income" && type == "deposit") ||
      (tabName == "expense" && type == "withdraw") ||
      (tabName == "asset" && (type == "buy" || type == "sell"));
  }

  List<DropdownMenuItem<String>> _dropDownMenuItems;

  @override
  void initState() {
    _dropDownMenuItems = buildAndGetDropDownMenuItems(categories);
    _category = _dropDownMenuItems[0].value;
    _moneyController.text = "0.00";
    _amountController.text = "1";
    if (item != null) {
      print(item.id);
      if (_isMatchType(item.type)) {
        _moneyController.text = item.money.toString();
        _amountController.text = item.amount.toString();
        _money = item.money.toString();
        _date = DateTime.parse(item.date);
        _amount = item.amount.toString();
        _category = item.category;
        _note = item.note;
      }
      if (item.type == "buy" || item.type == "sell") {
        _assetType = item.type;
      }
    }
    super.initState();
  }

  List<DropdownMenuItem<String>> buildAndGetDropDownMenuItems(List categories) {
    List<DropdownMenuItem<String>> items = new List();
    for (String category in categories) {
      items.add(new DropdownMenuItem(value: category, child: new Text(category)));
    }
    return items;
  }

  Future _selectDate() async {
    DateTime picked = await showDatePicker(
        context: context,
        initialDate: _date,
        firstDate: new DateTime(2000),
        lastDate: new DateTime(2100)
    );
    if(picked != null) setState(() => _date = picked);
  }

  final TextEditingController _moneyController = new TextEditingController();
  final TextEditingController _amountController = new TextEditingController();

  void _onSubmit() async {
    if (item == null) {
      TransactionList newTransaction = TransactionList(
        type: _getTransactionType(),
        money: double.parse(_money),
        amount: int.parse(_amount),
        date: _date.toIso8601String(),
        category: _category,
        note: _note,
      );
      await TransactionListProvider.db.create(newTransaction);
    } else {
      TransactionList updatedTransaction = TransactionList(
        id: item.id,
        type: _getTransactionType(),
        money: double.parse(_money),
        amount: int.parse(_amount),
        date: _date.toIso8601String(),
        category: _category,
        note: _note,
      );
      await TransactionListProvider.db.update(updatedTransaction);
    }
    Navigator.pop(context);
  }

  void _changeAssetType(assetType) {
    setState(() {
      _assetType = assetType;
    });
  }

  _buildAssetForm() {
    return <Widget>[
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          FlatButton(
            color: _assetType == "buy" ? Colors.red : Colors.white,
            textColor: _assetType == "buy" ? Colors.white : Colors.red,
            onPressed: () => _changeAssetType("buy"),
            child: Text("Buy")
          ),
          FlatButton(
            color: _assetType == "sell" ? Colors.green : Colors.white,
            textColor: _assetType == "sell" ? Colors.white : Colors.green,
            onPressed: () => _changeAssetType("sell"),
            child: Text("Sell")
          )
        ],
      ),
      TextField(
        onChanged: (String str) {
          setState(() {
            _money= str;
          });
          _moneyController.text = str;
        },
        controller: _moneyController,
      ),
      TextField(
        onChanged: (String str) {
          setState(() {
            _amount = str;
          });
          _amountController.text = str;
        },
        controller: _amountController,
      ),
      FlatButton(
        onPressed: _selectDate,
        child: Text(
          DateFormat('dd-MM-yyyy').format(_date)
        )
      ),
      DropdownButton(
        value: _category,
        items: _dropDownMenuItems,
        onChanged: (String selectedValue) {
          setState(() {
            _category = selectedValue;
          });
        },
      ),
      TextField(
        decoration: new InputDecoration(hintText: "Note"),
        onChanged: (String str) {
          setState(() {
            _note= str;
          });
        },
      ),
      RaisedButton(
        onPressed: _onSubmit,
        child: Text("Save"),
        color: Colors.orange,
        textColor: Colors.white
      )
    ];
  }

  _buildForm() {
    return <Widget>[
      TextField(
        onChanged: (String str) {
          setState(() {
            _money= str;
          });
          _moneyController.text = str;
        },
        controller: _moneyController,
      ),
      FlatButton(
        onPressed: _selectDate,
        child: Text(
          DateFormat('dd-MM-yyyy').format(_date)
        )
      ),
      DropdownButton(
        value: _category,
        items: _dropDownMenuItems,
        onChanged: (String selectedValue) {
          setState(() {
            _category = selectedValue;
          });
        },
      ),
      TextField(
        decoration: new InputDecoration(hintText: "Note"),
        onChanged: (String str) {
          setState(() {
            _note= str;
          });
        },
      ),
      RaisedButton(
        onPressed: _onSubmit,
        child: Text("Save"),
        color: Colors.orange,
        textColor: Colors.white
      )
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: tabName == "asset" ? _buildAssetForm() : _buildForm() ,
      )
    );
  }
}
